import { IAppMode } from '../enums';
import { IAnnotation, INote } from '../interfaces';

// Mode Actions
export const UPDATE_MODE = 'UPDATE_MODE';

export const updateMode = (mode: IAppMode) => {
    return {
        type: UPDATE_MODE,
        mode,
    }
};

// Annotations Actions
export const UPDATE_ANNOTATIONS = 'UPDATE_ANNOTATIONS';

export const updateAnnotations = (annotations: IAnnotation[]) => {
    return {
        type: UPDATE_ANNOTATIONS,
        annotations,
    }
};

// Note Actions
export const UPDATE_NOTES = 'UPDATE_NOTES';

export const updateNotes = (notes: INote[]) => {
    return {
        type: UPDATE_NOTES,
        notes,
    }
};
