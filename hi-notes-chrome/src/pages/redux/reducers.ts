import { combineReducers } from 'redux';
import {
    UPDATE_MODE,
    UPDATE_ANNOTATIONS,
    UPDATE_NOTES,
} from './actions';
import { IAppMode } from '../enums';
import { IAnnotation, INote } from '../interfaces';

const mode = ((state = IAppMode.writer, action: any) => {
    switch (action.type) {
        case UPDATE_MODE:
            return action.mode
        default:
            return state
    }
});

const annotations = ((state: IAnnotation[] = [], action: any) => {
    switch (action.type) {
        case UPDATE_ANNOTATIONS:
            return action.annotations
        default:
            return state
    }
});

const notes = ((state: INote[] = [], action: any) => {
    switch (action.type) {
        case UPDATE_NOTES:
            return action.notes
        default:
            return state
    }
});


export default combineReducers({
    mode,
    annotations,
    notes,
});
