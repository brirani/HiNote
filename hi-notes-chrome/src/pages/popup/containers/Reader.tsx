import * as React from 'react';
import * as firebase from 'firebase';
import { INote, IAnnotation } from '../../interfaces';
import Profiles from '../components/Profiles';

interface IReaderProps {
    notes: INote[],
    onClick: (annotations: IAnnotation[]) => void,
    refreshNotes: () => void,
}

interface IReaderState {
    url: string,
}

class Reader extends React.Component <IReaderProps, IReaderState> {

    constructor(props) {
        super(props);
        this.state = {
            url: '',
        }
        let url = '';
        window.chrome.tabs.query({ 
            active: true,
            currentWindow: true,
        }, tabs => {
            if (tabs[0] && tabs[0].url) url = tabs[0].url;
            this.setState({ url, });
        });
    }

    onClickUp(note: INote) {
        const collectionRef = firebase.firestore().collection('/notes');
        collectionRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                if (note.author == doc.data().author && note.date == doc.data().date) {
                    collectionRef.doc(doc.id).update({
                        score: note.score + 1,
                    })
                    .then(result => {
                        this.props.refreshNotes();
                    });
                }
            });
        });
    }

    onClickDown(note: INote) {
        const collectionRef = firebase.firestore().collection('/notes');
        collectionRef.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                if (note.author == doc.data().author && note.date == doc.data().date) {
                    collectionRef.doc(doc.id).update({
                        score: note.score - 1,
                    })
                    .then(result => {
                        this.props.refreshNotes();
                    });
                }
            });
        });
    }

    formatProfiles() {
        let notes = this.props.notes.filter(note => note.url == this.state.url);
        notes = notes.sort((a, b) => b.score - a.score);
        return notes.map(note => {
            return(
                <Profiles
                    author={note.author}
                    annotations={note.annotations}
                    date={note.date}
                    score={note.score}
                    onClick={() => this.props.onClick(note.annotations)}
                    onClickUp={() => this.onClickUp(note)}
                    onClickDown={() => this.onClickDown(note)}
                />
            );
        });
    }

    render() {
        const profiles = this.formatProfiles();
        return(
            <div className='container'>
                {
                    profiles.length == 0
                    &&
                    <p>Looks like you're the first one here, be the first to annotate this page!</p>
                }
                {profiles}
            </div>
        );
    }

}

export default Reader;