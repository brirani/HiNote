import * as React from 'react';
import * as firebase from 'firebase';
import Note from '../components/Note';
import { IAnnotation } from '../../interfaces';
import * as ClipboardJS from 'clipboard';
import './Writer.css';

interface IWriterProps {
    annotations: IAnnotation[],
    handleDeleteAnnotation: (string) => void,
    refreshWriter: () => void,
}

interface IWriterState {
    signedIn: boolean,
    email: string,
    password: string,
    isSaving: boolean,
}

class Writer extends React.Component <IWriterProps, IWriterState> {

    constructor(props) {
        super(props);
        new ClipboardJS('.icon-copy');
        this.state = {
            signedIn: false,
            email: '',
            password: '',
            isSaving: false,
        }
        this.handleLogin();
    }

    formatAnnotations(annotations: IAnnotation[]) {
        return annotations.map(annotation => {
            return(
                <Note
                    text={annotation.text}
                    lineCount={3}
                    onCopy={() => {}}
                    onDelete={() => this.props.handleDeleteAnnotation(annotation.text)}
                />
            );
        })
    }

    handleLogin() {
        let { email, password } = this.state;
        if(localStorage.getItem('email')) email = localStorage.getItem('email') || '';
        if(localStorage.getItem('password')) password = localStorage.getItem('password') || '';
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => {
            localStorage.setItem('email', email);
            localStorage.setItem('password', password);
            this.setState({ signedIn: true });
        });
    }

    handleSaveNote() {
        this.setState({ isSaving: true });
        const collectionRef = firebase.firestore().collection('/notes');
        const user = firebase.auth().currentUser;
        window.chrome.tabs.query({ 
            active: true,
            currentWindow: true,
        }, tabs => {
            let url = ''
            if (tabs[0] && tabs[0].url) url = tabs[0].url;
            collectionRef.add({
                annotations: this.props.annotations,
                author: user ? user.email : 'error',
                date: new Date().toLocaleString(),
                url,
                score: 0,
            }).then((result) =>{
                this.setState({ isSaving: false });
                this.props.refreshWriter();
            });
        });
    }

    render() {
        const { annotations } = this.props;
        return(
            <div>
                {
                    this.state.signedIn ?
                    <div>
                        <button 
                            className='btn btn-success button-save'
                            onClick={e => this.handleSaveNote()}
                        >
                            {
                                this.state.isSaving ?
                                <p>Saving...</p>
                                :
                                <p>Save Note</p>
                            }
                        </button>
                        {this.formatAnnotations(annotations)}
                    </div>
                    :
                    <div className='container'>
                        <div className="input-group input-group-sm mb-3">
                            <input 
                                className='form-control'
                                type='text'
                                placeholder='email'
                                onChange={e => this.setState({ email: e.target.value })}
                                value={this.state.email}
                                aria-label="Small"
                                aria-describedby="inputGroup-sizing-sm"
                            />
                        </div>
                        <div className="input-group input-group-sm mb-3">
                            <input 
                                className='form-control'
                                type='password'
                                placeholder='password'
                                onChange={e => this.setState({ password: e.target.value })}
                                value={this.state.password}
                                aria-label="Small"
                                aria-describedby="inputGroup-sizing-sm"
                            />
                        </div>
                        <button
                            className='btn btn-primary'
                            onClick={e => this.handleLogin()}
                        >
                            Login
                        </button>
                    </div>
                }
            </div>
        );
    }

}

export default Writer;