import * as React from 'react';
import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Profiles.css';
import { IAnnotation } from '../../interfaces';

interface IProfilesProps {
    author: string,
    date: string,
    annotations: IAnnotation[],
    score: number,
    onClick: () => void,
    onClickUp: () => void,
    onClickDown: () => void,
}

class Profiles extends Component <IProfilesProps> {
    
    constructor (props: IProfilesProps) {
        super(props);
    }

    render () {
        const { author, date, score } = this.props;
        return(
            <div className='profiles-container'>
                <div className="profiles-text">
                    <p>{author}</p>
                    <p>{date}</p>
                </div>
                <button className='btn btn-info highlight-button' onClick={event => this.props.onClick()}>
                    <FontAwesomeIcon icon='highlighter' />
                </button>
                <div className="profiles-buttons">
                    <button className='btn btn-success' onClick={event => this.props.onClickUp()} >
                        <FontAwesomeIcon icon='thumbs-up' />
                    </button>
                    <div>
                        <p className='score-text'>{score}</p>
                    </div>
                    <button className='btn btn-danger' onClick={event => this.props.onClickDown()} >
                        <FontAwesomeIcon icon='thumbs-down' />
                    </button>
                </div>
            </div>
        );
    }
}

export default Profiles;
