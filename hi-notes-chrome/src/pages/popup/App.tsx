import * as React from 'react';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Writer from './containers/Writer';
import Reader from './containers/Reader';
import { updateMode, updateNotes, updateAnnotations, } from '../redux/actions';
import { IReduxStore, IAnnotation, INote } from '../interfaces';
import { IAppMode } from '../enums';
import './App.css';
import '../../static/bootstrap.min.css';

let app = firebase.initializeApp({
    apiKey: "AIzaSyBcJc213eRo8xutWDlpMFc2qHsHavTbX6o",
    authDomain: "hinote-efd65.firebaseapp.com",
    databaseURL: "https://hinote-efd65.firebaseio.com",
    projectId: "hinote-efd65",
    storageBucket: "hinote-efd65.appspot.com",
    messagingSenderId: "139007776581",
    appId: "1:139007776581:web:6a72e6eae7fca8439c902c"
});

interface IAppProps {
    mode: IAppMode,
    annotations: IAnnotation[],
    notes: INote[],
    updateMode: (mode: IAppMode) => void,
    updateNotes: (notes: INote[]) => void,
    updateAnnotations: (annotations: IAnnotation[]) => void,
}

class App extends React.Component <IAppProps> {

    constructor(props) {
        super(props);
        this.refreshNotes();
    }

    refreshNotes() {
        const collectionRef = firebase.firestore().collection('/notes');
        collectionRef.get()
        .then(queries => {
            const notes = queries.docs.map(query => query.data()) as INote[];
            this.props.updateNotes(notes);
        });
    }

    refreshPage() {
        chrome.tabs.query({active: true, currentWindow: true}, tabs => {
            if (!tabs || !tabs[0] || !tabs[0].id) return;
            var code = 'window.location.reload();';
            chrome.tabs.executeScript(tabs[0].id, { code, });
        });
    }

    handleDeleteAnnotation(text: string) {
        const annotations = this.props.annotations.filter(annotation => annotation.text != text);
        this.props.updateAnnotations(annotations);
    }

    handleReadNote(annotations: IAnnotation[]) {
        if (this.props.annotations.length != 0) {
            this.refreshPage();
            setTimeout(() => {
                this.props.updateAnnotations(annotations);
            }, 1000);
        } else {
            this.props.updateAnnotations(annotations);
        }
    }

    handleWriteToggle() {
        this.props.updateMode(IAppMode.writer);
        this.props.updateAnnotations([]);
        this.refreshPage();
    }

    handleReadToggle() {
        this.props.updateMode(IAppMode.reader);
        this.props.updateAnnotations([]);
    }

    render() {
        const { mode, annotations } = this.props;
        return(
            <div>
                <div className='button-headers'>
                    <button
                        className='btn btn-light button-writer'
                        onClick={e => this.handleWriteToggle()}
                    >
                        <div>
                            <h4 className='button-text'>Writer</h4>
                            <FontAwesomeIcon icon='pencil-alt' />
                        </div>
                    </button>
                    <button
                        className='btn btn-light'
                        onClick={e => this.handleReadToggle()}
                    >
                        <div>
                            <h4 className='button-text'>Reader</h4>
                            <FontAwesomeIcon icon='copy' />
                        </div> 
                    </button>
                </div>
                {
                    mode == IAppMode.writer ?
                    <Writer
                        annotations={annotations}
                        handleDeleteAnnotation={this.handleDeleteAnnotation.bind(this)}
                        refreshWriter={(() => this.props.updateAnnotations([])).bind(this)}
                    />
                    :
                    <Reader 
                        notes={this.props.notes}
                        onClick={this.handleReadNote.bind(this)}
                        refreshNotes={this.refreshNotes.bind(this)}
                    />
                }
            </div>
        );
    }
}

const mapStateToProps = (state: IReduxStore) => ({
    mode: state.mode,
    annotations: state.annotations,
    notes: state.notes,
});

const mapDispatchToProps = (dispatch: any) => ({
    updateMode: (mode: IAppMode) => dispatch(updateMode(mode)),
    updateNotes: (notes: INote[]) => dispatch(updateNotes(notes)),
    updateAnnotations: (annotations: IAnnotation[]) => dispatch(updateAnnotations(annotations)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);