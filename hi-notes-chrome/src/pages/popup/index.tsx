import { render } from 'react-dom';
import { Provider } from 'react-redux';
import * as React from 'react';
import { Store } from 'webext-redux';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTrash, faCopy, faPencilAlt, faCheck, faThumbsUp, faThumbsDown, faHighlighter } from '@fortawesome/free-solid-svg-icons';
import App from './App';

library.add(faTrash, faCopy, faPencilAlt, faCheck, faThumbsUp, faThumbsDown, faHighlighter);

const store = new Store({
    portName: 'HiNote'
});

store.ready().then(() => {
    let mountNode = document.createElement('div');
    mountNode.id = 'root';
    document.body.appendChild(mountNode);
    render(
        <Provider store={store}>
            <App/>
        </Provider>
    , mountNode);
});