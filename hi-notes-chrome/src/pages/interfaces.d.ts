import { IAppMode } from './enums';

export interface IAnnotation {
    start: number[],
    end: number,
    text: string,
    comment: string,
}

export interface INote {
    annotations: IAnnotation[],
    author: string,
    date: string,
    url: string,
    score: number,
}

export interface IReduxStore {
    mode: IAppMode,
    annotations: IAnnotation[],
    notes: INote[],
    login: {
        email: string,
        password: string,
    }
}