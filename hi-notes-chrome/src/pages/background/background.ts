import { wrapStore } from 'webext-redux';
import ContextMenu from './contextMenus';
import store from '../redux/store';
import { updateAnnotations } from '../redux/actions';
import { IAppMode } from '../enums';

const startupScripts = () => {
    wrapStore(store, {
        portName: 'HiNote',
    });
    store.subscribe(() => {
        const { annotations, mode } = store.getState();
        chrome.browserAction.setBadgeText({
            text: `${annotations.length}`,
        });
        if (mode == IAppMode.reader) {
            chrome.tabs.query({active: true, currentWindow: true}, tabs => {
                if (!tabs || !tabs[0] || !tabs[0].id) return;
                chrome.tabs.sendMessage(tabs[0].id, {
                    annotations: JSON.stringify(annotations),
                });
            });
        }
    });
    chrome.runtime.onMessage.addListener((request, sender) => {
        const state = store.getState();
        if (chrome.runtime.id == sender.id && state.mode == IAppMode.writer) {
            const start = JSON.parse(request.start);
            const end = JSON.parse(request.end);
            const text = request.text;
            const { annotations } = store.getState();
            annotations.push({
                start,
                end,
                text,
                comment: '',
            });
            store.dispatch(updateAnnotations(annotations));
        }
    });
    chrome.tabs.onSelectionChanged.addListener((tabId, selectInfo) => {
        store.dispatch(updateAnnotations([]));
    });
    const contextMenu = new ContextMenu(store);
    contextMenu.initContextMenu();
}

chrome.runtime.onInstalled.addListener(() => {
    startupScripts();
});

chrome.runtime.onStartup.addListener(() => {
    startupScripts();
});
