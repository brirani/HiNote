import { Store } from 'webext-redux';
import { IReduxStore } from '../interfaces';
import { updateAnnotations } from '../redux/actions';

class ContextMenu {
    store: Store;

    constructor (store) {
        this.store = store;
    }

    public initContextMenu () {
        chrome.contextMenus.create({
            id: "HiNoteHighlight",
            title: "Highlight Section",
            contexts: ["selection"],
        });
        chrome.contextMenus.onClicked.addListener((clickData) => {
            if (clickData.menuItemId == "HiNoteHighlight" && clickData.selectionText) {
                const state: IReduxStore = this.store.getState();
                const selection = window.getSelection();
                if (!selection) return;
                const { annotations } = state;
                console.log(selection);
            }
        });
    }

}

export default ContextMenu;
