import { IAnnotation } from "../interfaces";

class ContentScript {
    
    public initContentScript () {
        document.addEventListener('copy', (event: ClipboardEvent) => {
            const selection = window.getSelection();
            if (!selection || !selection.anchorNode) return;
            let currentNode = selection.anchorNode;
            let anchors: number[] = [];
            while (currentNode) {
                anchors.push(this.findChildIndex(currentNode));
                currentNode = currentNode.parentNode as Node;
            }
            let focuses: number = this.findChildIndex(selection.focusNode)
            anchors.reverse();
            this.highlightText(anchors, focuses);
            chrome.runtime.sendMessage({
                start: JSON.stringify(anchors),
                end: JSON.stringify(focuses),
                text: selection.toString()
            });
        });
        chrome.runtime.onMessage.addListener((request, sender) => {
            if (chrome.runtime.id == sender.id) {
                if (request.annotations) {
                    const annotations: IAnnotation[] = JSON.parse(request.annotations);
                    for (let annotation of annotations) {
                        this.highlightText(annotation.start, annotation.end);
                    }
                }
            }
        });
    }

    private highlightText(start, end) {
        const anchors = start;
        const focuses = end;
        let currentNode = document as Node;
        let focusNode: Node | null = null;
        for (let i = 1; i < anchors.length; i++) {
            if (i == anchors.length - 1) focusNode = currentNode.childNodes[focuses];
            currentNode = currentNode.childNodes[anchors[i]];
        }
        if (currentNode === focusNode && currentNode.nodeType === 3) {
            const mark = document.createElement('mark');
            mark.appendChild(document.createTextNode(currentNode.nodeValue || ''));
            if (currentNode.parentNode) (currentNode.parentNode as Node).replaceChild(mark, currentNode);
            return;
        }
        while (currentNode && currentNode != focusNode) {
            let oldNode = currentNode;
            if (currentNode.nodeType === 3) {
                const mark = document.createElement('mark');
                mark.appendChild(document.createTextNode(currentNode.nodeValue || ''));
                if (currentNode.parentNode) (currentNode.parentNode as Node).replaceChild(mark, currentNode);
                currentNode = mark.nextSibling as Node;
            } else {
                let htmlCurrentNode = currentNode as HTMLElement;
                htmlCurrentNode.innerHTML = `<mark>${htmlCurrentNode.innerHTML}</mark>`;
                currentNode = oldNode.nextSibling as Node;
            }
        }
    }

    private findChildIndex(currentNode) {
        let parentNode = currentNode.parentNode as Node;
        if (!parentNode || !parentNode.childNodes) return 0;
        for (let i = 0; i < parentNode.childNodes.length; i++) {
            if (parentNode.childNodes[i] == currentNode) {
                return i;
            }
        }
        return 0;
    }

}

const contentScript = new ContentScript();
contentScript.initContentScript();
