# Introduction

Calhacks note taking chrome extension built in TypeScript, React, Webpack

# Building & Testing

1. `yarn install` to install dependancies
2. `yarn run build` to generate a fast development build for debugging in the `dist` folder
3. Navigate to `chrome://extensions/` URL in Chrome
4. Turn on `developer mode` in the top right hand corner
5. Click on `Load unpacked`
6. Select the entire `dist` folder