# HiNote


## Run (local) everytime you want to use this environment -- run from within src dir
***************
1. `workon hinote`
1. `python manage.py makemigrations`
1. `python manage.py migrate`
1. `python manage.py runserver`

## Set-up
**********
1. Create environment from within HiNote repo
    1. `pip install virtualenv`
    1. `pip install virtualenvwrapper-win`
    1. `mkvirtualenv hinote`
    1. `pip install -r requirements.txt` - make sure this steps is done after the virtual environment is set up.
