from django.urls import path
from django.conf.urls import url


from . import views
app_name = 'landing'
urlpatterns = [
    path('', views.index, name='index'),
    path('token/', views.token_auth, name='token_auth'),
]
