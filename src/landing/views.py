import os
from django.shortcuts import render
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore


if (not len(firebase_admin._apps)):
    cred = credentials.Certificate("static/credentials/credentials.json")
    firebase_admin.initialize_app(cred, {
      'projectId': 'hinote-efd65',
    })

db = firestore.client()
authenticated = False

class Note:
    def __init__(self):
        self.content = []
        self.author = ""
        self.date = ""
        self.url = ""
        self.summary = ""
        self.score = ""

def token_auth(request):
    global authenticated
    authenticated = True
    print(authenticated)
    return index(request)

# Create your views here.
def index(request):
    global authenticated
    notes = db.collection(u'notes')
    parsed_notes = []
    print(authenticated)
    if authenticated:
        for note in notes.stream():
            parsed_note = Note()
            parsed_note.author = note.to_dict()["author"]
            parsed_note.date = note.to_dict()["date"]
            parsed_note.url = note.to_dict()["url"]
            texts = note.to_dict()['annotations']
            parsed_note.summary = texts[0]['text']
            parsed_note.score = note.to_dict()["score"]
            for text in texts:
                parsed_note.content.append(text['text'])
            parsed_note.content.pop(0)
            parsed_notes.append(parsed_note)
            print(parsed_note.content)
            parsed_notes.sort(key=lambda x: x.score, reverse=True)

    return render(request, 'landing/index.html', {'parsed_notes':parsed_notes, 'auth':authenticated})
